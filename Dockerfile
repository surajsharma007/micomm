FROM registry.gitlab.com/harukanetwork/oss/harukaaya:dockerstation

RUN git clone https://gitlab.com/surajsharma007/micomm.git -b master /data/HarukaAya

COPY ./config.yml /data/HarukaAya

WORKDIR /data/HarukaAya

CMD ["python", "-m", "haruka"]
